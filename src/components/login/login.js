import {  useState } from "react"
import axios from "axios"
import {Button,  InputGroup,  FormControl} from 'react-bootstrap';
import './login.css'


const authTokenServer = process.env.REACT_APP_SERVER_ADDRESS
const Login = ({setAuthToken})=>{

    const [form, setForm]=useState({
        uname:'lucky',
        upass:'pass'
    })

    const login = ()=>{

        let config = {
            headers: {
                'Content-type': 'multipart/form-data'
            }
        }

        var bodyFormData = new FormData();
        bodyFormData.append('username', form.uname);
        bodyFormData.append('password', form.upass);

  
        axios.post(`${authTokenServer}/login`,
        bodyFormData,
        config
        ).then(function (response) {
            setAuthToken(response.data.token)
            
          })
          .catch(function (error) {
            console.log('wrong creds',error);
           })
    }

    return(
        <div className="login_box">
            <center><h3>eCash Wallet</h3></center>
              <InputGroup className="mb-3">
    <InputGroup.Prepend>
      <InputGroup.Text id="basic-addon1">uname</InputGroup.Text>
    </InputGroup.Prepend>
    <FormControl
      placeholder="Username"
      aria-label="Username"
      value={form.uname}
      onInput={e => setForm({...form, uname:e.target.value})}
      aria-describedby="basic-addon1"
    />
  </InputGroup>
  <InputGroup className="mb-3">
    <InputGroup.Prepend>
      <InputGroup.Text id="basic-addon2">upass</InputGroup.Text>
    </InputGroup.Prepend>
    <FormControl
    value={form.upass}
    onInput={e => setForm({...form, upass:e.target.value})}
      placeholder="Password"
      aria-label="Password"
      aria-describedby="basic-addon2"
    />
  </InputGroup>
  <Button onClick={login}>Login</Button>
<br/><br/>
  <ul className="login_box_users">
        <li>
        user1:
        uname: lucky
        pass:  pass
        </li>
        <li>
        user2:
        uname: lucky2
        pass:  pass
        </li>
        <li>
        user3:
        uname:  lucky3
        pass:  pass
        </li>
    </ul>
   
        </div>
    )
    
}

export default Login
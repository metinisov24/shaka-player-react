module.exports ={ 
    getParam:(param)=>{
        return new URLSearchParams(window.location.search).get(param);
      },
    positiveMessage:(msgTitle, msgBody)=>{
       // alert(msgTitle)
    /*   Swal.fire(
        msgTitle || '',
        msgBody || '',
        'info'
      ) */
    },
    negativeMessage:(msgTitle, msgBody)=>{
        alert(msgTitle)
      /*   Swal.fire(
          msgTitle || '',
          msgBody || '',
          'warning'
        ) */
      },
    initApp:(shaka, initPlayer, params)=>{
      if (window.player){
        window.player.destroy()
        
      }

      if (window.charging){
        clearInterval(window.charging)
      }
      setTimeout(function() {
      }, 1500);


        //shaka.log.setLevel(shaka.log.Level.V1);
        // Install built-in polyfills to patch browser incompatibilities.
        shaka.log.setLevel(shaka.log.Level.DEBUG);
        shaka.polyfill.installAll();
      
        // Check to see if the browser supports the basic APIs Shaka needs.
        if (shaka.Player.isBrowserSupported()) {
          
          // Everything looks good!
          initPlayer.apply(this, params);
        } else {
          // This browser does not have the minimum set of APIs we need.
          console.error('Browser not supported!');
        }
      },
    onError:(error)=>{
        // Extract the shaka.util.Error object from the event.
     
        console.error('Error code', error.code, 'object', error);
    
      }
    
    }
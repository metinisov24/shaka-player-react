import { useEffect, useState } from "react"
import axios from "axios"
import Button from 'react-bootstrap/Button';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import Login from '../login/login'

const utils = require('./utils.js');

const shaka = window.shaka
window.canPlay = true
var reattemptToPlay=false
const authTokenServer = process.env.REACT_APP_SERVER_ADDRESS
console.log('process :>> ', process);
const manifestUri =
    'https://storage.googleapis.com/shaka-demo-assets/sintel-widevine/dash.mpd';
const licenseServer = 'https://cwip-shaka-proxy.appspot.com/header_auth';



const Player = ()=>{
const [authToken, setAuthToken] = useState('')


useEffect(() => {
  reattemptToPlay=true
  console.log('auth token: ',authToken);
  },[authToken]);

  const playVideo = (e)=>{

    switch (e.target.value){
      case 'free':
        utils.initApp(shaka, initPlayerFree, [false])
        return
      case 'continious_charge':
          utils.initApp(shaka, initPlayerContiniousCharging, [authToken])
     return
          case 'one_time':
            utils.initApp(shaka, initPlayerInitialCharging, [authToken])
    return
    default :{

    }
    }

    console.log('arg :>> ',e );
  }

  const bankruptUser = ()=>{
    axios.get(`${authTokenServer}/authorize/bankrupt`,{
      headers:{'Authorization': `Bearer ${authToken}`}
    })
    .then(function (response) {
      utils.positiveMessage("User bankrupted...")
    })
    .catch(function (error) {
      utils.negativeMessage("Ussue with bankrupting: ", error)
     })
  }

return (
    <div>
      <ButtonToolbar>
        <Button variant="success" onClick={playVideo} value="free">Free Play</Button>
       &nbsp; <Button variant="warning" onClick={playVideo} disabled={!authToken} value="continious_charge">Continuous Charge</Button>
       &nbsp;  <Button variant="warning" onClick={playVideo} disabled={!authToken} value="one_time">One Time Charge</Button>

      </ButtonToolbar><br/>
     <div id="video_container">
       </div> 
    <video
      id="video"
      width="640"
      poster="https:://shaka-player-demo.appspot.com/assets/poster.jpg"
      controls
      autoPlay
    ></video>

{
  authToken?
  <div>
<h3>Logged in,   <Button variant="success" onClick={bankruptUser}>Bankrupt the user</Button>
     </h3>

</div>
  :
<Login setAuthToken = {setAuthToken}></Login>
   
}
</div>  
)
}

export default Player

function initPlayerContiniousCharging(authToken) {
  
  // Create a Player instance.
  var video = document.getElementById('video');

  var player = new shaka.Player(video);
  
  window.player = player;
  var lastPlayTime=0;

  console.log('DRM info: ', player.drmInfo())
  //Set the authorization header for the encripted content
  player.getNetworkingEngine().registerRequestFilter(function (type, request) {
    // Only add headers to license requests:
    if (type !== shaka.net.NetworkingEngine.RequestType.LICENSE) return;
  //  console.log('Comes from the request filter: ', shaka.dash.ContentProtection.defaultKeySystems_);
    request.headers['CWIP-Auth-Header'] = "VGhpc0lzQVRlc3QK";
  })
 
 window.charging = setInterval(function(){ 
    let pbr = window.player.getStats()
    let amountToCharge=0.01
    

    console.log('pbr :>> ', pbr.playTime);


    if (((pbr.playTime - lastPlayTime)>=1) || reattemptToPlay){
        lastPlayTime = pbr.playTime
        axios.get(`${authTokenServer}/authorize/charge?amount=${amountToCharge}`,{
          headers:{'Authorization': `Bearer ${authToken}`}
        })
        .then(function (response) {
          reattemptToPlay = false
          window.canPlay=true
          video.play()
          console.log('Charged',response);
        })
        .catch(function (error) {
          reattemptToPlay = false
          window.canPlay=false
          console.log('Player stopped: ',error);
          video.pause()
          utils.negativeMessage("You are not allowed to see the video...")
         })
      }
    

    if (!window.canPlay){
      video.pause()
    }

  console.log('Licensed time :>> ', pbr.licenseTime);
   console.log('play time :>> ', pbr.playTime);
  }, 1000);

  player.configure({
    drm: {
      servers: {
        'com.widevine.alpha': licenseServer
      }
    }
  });
  console.log('player xx:>> ', player);
  // Try to load a manifest.
  // This is an asynchronous process.
  player.load(manifestUri).then(function () {
    // This runs if the asynchronous load is successful.
    console.log('The video has now been loaded!');
  }).catch(utils.onError); // onError is executed if the asynchronous load fails.
}


function initPlayerInitialCharging(authToken) {
  
  // Create a Player instance.
  var video = document.getElementById('video');
  var player = new shaka.Player(video);
  window.player = player;

  console.log('DRM info: ', player.drmInfo())
  //Set the authorization header for the encripted content
  player.getNetworkingEngine().registerRequestFilter(function (type, request) {
    // Only add headers to license requests:
    if (type !== shaka.net.NetworkingEngine.RequestType.LICENSE) return;
  //  console.log('Comes from the request filter: ', shaka.dash.ContentProtection.defaultKeySystems_);
   
  var authRequest = {
    uris: [`${authTokenServer}/authorize/charge?amount=0.25`],
    method: 'GET',
    headers:{'Authorization': `Bearer ${authToken}`
  },
  };

  var requestType = shaka.net.NetworkingEngine.RequestType.APP;

  return player.getNetworkingEngine().request(requestType, authRequest).promise.then(async function (response) {

  
      request.headers['CWIP-Auth-Header'] = "VGhpc0lzQVRlc3QK";
      utils.positiveMessage('Success :)', 'Enjoy the video...') 
  }).catch(function(s){
    utils.negativeMessage('Sorry, You can not see the video... (:')
  })

  })


  player.configure({
    drm: {
      servers: {
        'com.widevine.alpha': licenseServer
      }
    }
  });
  // Try to load a manifest.
  // This is an asynchronous process.
  player.load(manifestUri).then(function () {
    // This runs if the asynchronous load is successful.
    console.log('The video has now been loaded!');
  }).catch(utils.onError); // onError is executed if the asynchronous load fails.
}

/* function initPlayerInitialChargingWithBOPP(chargeOnlyDRM) {
  
  // Create a Player instance.
  var video = document.getElementById('video');
  var player = new shaka.Player(video);
  window.player = player;

  console.log('DRM info: ', player.drmInfo())
  //Set the authorization header for the encripted content
  player.getNetworkingEngine().registerRequestFilter(function (type, request) {
    // Only add headers to license requests:
    if (type != shaka.net.NetworkingEngine.RequestType.LICENSE) return;
  //  console.log('Comes from the request filter: ', shaka.dash.ContentProtection.defaultKeySystems_);
   
  var authRequest = {
    uris: [`${authTokenServer}/authorize/charge?amount=0.25`],
    method: 'GET',
    headers:{'Authorization': `Bearer ${utils.getParam("userToken")}`
  },
  };

  var requestType = shaka.net.NetworkingEngine.RequestType.APP;

  return player.getNetworkingEngine().request(requestType, authRequest).promise.then(async function (response) {

  let resonseJson = JSON.parse(shaka.util.StringUtils.fromUTF8(response.data))
      request.headers['CWIP-Auth-Header'] = "VGhpc0lzQVRlc3QK";
      utils.positiveMessage('Success :)', 'Enjoy the video...') 
  }).catch(function(s){
    utils.negativeMessage('Sorry :)', 'You can not see the video...')
  })

  })


  player.configure({
    drm: {
      servers: {
        'com.widevine.alpha': licenseServer
      }
    }
  });
  // Try to load a manifest.
  // This is an asynchronous process.
  player.load(manifestUri).then(function () {
    // This runs if the asynchronous load is successful.
    console.log('The video has now been loaded!');
  }).catch(utils.onError); // onError is executed if the asynchronous load fails.
} */
  
function initPlayerFree(authToken) {
    // Create a Player instance.
    var video = document.getElementById('video');
    var player = new shaka.Player(video);


    window.player = player;
  
    console.log('DRM info: ', player.drmInfo())
    //Set the authorization header for the encripted content
    player.getNetworkingEngine().registerRequestFilter(function (type, request) {
      // Only add headers to license requests:
      if (type !== shaka.net.NetworkingEngine.RequestType.LICENSE) return;
      request.headers['CWIP-Auth-Header'] = 'VGhpc0lzQVRlc3QK';
      return  
    })

    player.configure({
      drm: {
        servers: {
          'com.widevine.alpha': licenseServer
        }
      }
    });
    // Try to load a manifest.
    // This is an asynchronous process.
    player.load(manifestUri).then(function () {
      // This runs if the asynchronous load is successful.
      console.log('The video has now been loaded!');
    }).catch(utils.onError); // onError is executed if the asynchronous load fails.
  }
  

  
 

 
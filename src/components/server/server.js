import { useEffect, useState } from "react"
import axios from "axios"
import {Card, Button} from 'react-bootstrap'
const utils = require ('../player/utils')

const serverAddr = process.env.REACT_APP_SERVER_ADDRESS


const Server = ()=>{

const [loggedUsers, setUsers] = useState()

useEffect(() => {
    setInterval(() => {
        axios.get(`${serverAddr}/stats`)
    .then(res => {
        setUsers(res.data)
    })
      }, 500); 
  },[]);

const logOutUsers = ()=>{
    axios.get(`${serverAddr}/logout_users`)
    .then(function (response) {
      utils.positiveMessage("User bankrupted...")

    })
    .catch(function (error) {
      utils.negativeMessage("Ussue with bankrupting: ", error)
     })
  }

return (

<div>

  {
    loggedUsers && loggedUsers.length > 0?
    <div>
    <h3>Logged in Users:</h3>
    {
    loggedUsers.map(user =>{
    return(
      <Card style={{ width: '18rem', marginTop:'0.1em' }}>
      <Card.Body>
      <Card.Title>User: <b>{user.name}</b></Card.Title>
      <Card.Subtitle className="mb-2 text-muted">Wallet amount: <b>{Number((user.wallet).toFixed(3))}</b></Card.Subtitle>
      </Card.Body>
      </Card>   
    )})
    }

   <Button variant="success" onClick={logOutUsers} >Log out the users...</Button>
 

   </div>
    : 
    <div>Nobody logged in...</div>
  }


</div>
)
}

export default Server
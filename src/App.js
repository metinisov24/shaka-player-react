import React from 'react';
import { MemoryRouter, Switch, Route } from 'react-router-dom';

import Jumbotron from 'react-bootstrap/Jumbotron';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import { LinkContainer } from 'react-router-bootstrap';

import Server from './components/server/server'
import Player from './components/player/player'

import './App.css';



const App = () => (
  <MemoryRouter>
    <Container className="p-3">
      <Jumbotron>
      <h2>

<ButtonToolbar className="custom-btn-toolbar">
<LinkContainer to="/server">
    <Button>Server</Button>
  </LinkContainer>
  <LinkContainer to="/">
    <Button>Players</Button>
  </LinkContainer>

</ButtonToolbar>
</h2>
        <h2>
          <Switch>
          <Route path="/server">
            
            <Server />
            </Route>
            <Route path="/">
            <Player /> 
            </Route>
          
            
          </Switch>
        </h2>

      </Jumbotron>
    </Container>
  </MemoryRouter>
);

export default App;
